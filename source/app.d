import std.stdio;
import std.conv;
import std.datetime;
import std.math;
import std.algorithm;

double count_of_op = 1e9;
int res_size = 12;

void main()
{
	writeln("--------------------------------------------------------------------------------------------------------------");
	//+ int
	StopWatch sw;
	TickDuration last = TickDuration.from!"seconds"(0);
	int a = 0;
	double [int] results;

	sw.start();
	for(int i = 0; i < count_of_op; i++)
		a = i + 12312324;
	sw.stop();

	double plus_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[0] = plus_in_sec;

	//- int
	last = sw.peek();

	sw.start();
	for(int i = 0; i < count_of_op; i++)
		a = i - 271237732;
	sw.stop();

	double minus_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[1] = minus_in_sec;

	//* int
	sw.start();
	for(int i = 0; i < count_of_op; i++)
		a = i * 29219;
	sw.stop();

	double mult_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[2] = mult_in_sec;

	/// int
	sw.start();
	for(int i = 1; i <= count_of_op; i++)
		a = 152312131 / i;
	sw.stop();

	double div_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[3] = div_in_sec;

	//+ long
	long b;
	last = sw.peek();

	sw.start();
	for(long i = 0; i < count_of_op; i++)
		b = 912317876864327231 + i;
	sw.stop();

	minus_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[4] = minus_in_sec;

	//- long
	last = sw.peek();

	sw.start();
	for(long i = 0; i < count_of_op; i++)
		b = i - 271237686786437732;
	sw.stop();

	minus_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[5] = minus_in_sec;

	//* long
	sw.start();
	for(long i = 0; i < count_of_op; i++)
		b = 152213123731 * i;
	sw.stop();

	mult_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[6] = mult_in_sec;

	/// long
	sw.start();
	for(long i = 1; i <= count_of_op; i++)
		b = 152314352121312131 / i;
	sw.stop();

	div_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[7] = div_in_sec;


	//+ double
	double c;
	last = sw.peek();

	sw.start();
	for(double i = 0; i < count_of_op; i+=1)
		c = 91231787.6864327231 + i;
	sw.stop();

	minus_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[8] = minus_in_sec;

	//- double
	last = sw.peek();

	sw.start();
	for(double i = 0; i < count_of_op; i+=1)
		c = i - 271237.686786437732;
	sw.stop();

	minus_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[9] = minus_in_sec;

	//* double
	sw.start();
	for(double i = 0; i < count_of_op; i+=1)
		c = 152213.123731 * i;
	sw.stop();

	mult_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[10] = mult_in_sec;

	/// double
	sw.start();
	for(double i = 1; i <= count_of_op; i+=1)
		c = 15231435.2121312131 / i;
	sw.stop();

	div_in_sec = 1000000000.0 / (sw.peek() - last).hnsecs;
	results[11] = div_in_sec;


	//writeln
	double max_el = 0;
	for(int i = 0; i < res_size; i++)
	{
		results[i] *= 1e7;
		max_el = max(max_el, results[i]);
	}
	double [int] percent;
	string [int] x;
	for(int i = 0; i < res_size; i++)
	{
		percent[i] = results[i] * 100.0 / max_el;
		x[i] = "";
		int q = 0;
		while(q + 2 <= percent[i])
		{
			q += 2;
			x[i] ~= "X";
		}
		while(x[i].length < "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     ".length)
			x[i] ~= " ";
		for(int j = 0; j < ("3.81125e+08".length - results[i].to!string.length); j++)
			x[i] = " " ~ x[i];
	}
	writeln("+     int        ", results[0], "     ", x[0], round(percent[0]), "%");
	writeln("-     int        ", results[1], "     ", x[1], round(percent[1]), "%");
	writeln("*     int        ", results[2], "     ", x[2], round(percent[2]), "%");
	writeln("/     int        ", results[3], "     ", x[3], round(percent[3]), "%");
	writeln("+     long       ", results[4], "     ", x[4], round(percent[4]), "%");
	writeln("-     long       ", results[5], "     ", x[5], round(percent[5]), "%");
	writeln("*     long       ", results[6], "     ", x[6], round(percent[6]), "%");
	writeln("/     long       ", results[7], "     ", x[7], round(percent[7]), "%");
	writeln("+     double     ", results[8], "     ", x[8], round(percent[8]), "%");
	writeln("-     double     ", results[9], "     ", x[9], round(percent[9]), "%");
	writeln("*     double     ", results[10], "     ", x[10], round(percent[10]), "%");
	writeln("/     double     ", results[11], "     ", x[11], round(percent[11]), "%");
	writeln("--------------------------------------------------------------------------------------------------------------");
}
